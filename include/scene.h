#ifndef SCENE_H
#define SCENE_H

#include <GL/gl.h>
#include "../include/scanpgm.h"
#include "../include/geometry.h"

#include "../include/create_object.h"


#define TAILLE_TAB_RANDOM 50
//int TAILLE_TAB_RANDOM = 15;


typedef struct Tree {
    Point3D positionTree;
    float sizeX;
    float sizeY; 
} Tree;


// Fonction de dessin d'un repere. L'axe x est rouge, l'axe y vert et l'axe z bleu.


Tree createTree(Point3D positionTree, float sizeX, float sizeY);


void glDrawRepere(float length);
void glDrawWater(PGMImage* pgm, float hauteur, GLuint textureEau[1], Sun* sun);

void initTabRandom(int tabRandomX[TAILLE_TAB_RANDOM], int tabRandomY[TAILLE_TAB_RANDOM], int tabSize[TAILLE_TAB_RANDOM], timacParams* timac);
void displayTree(Tree* arbre, timacParams* timac, PGMImage* pgm, Camera* cam, GLuint textureArbre[1], Sun* sun);

#endif