#ifndef QUADTREE_H
#define QUADTREE_H
#include "geometry.h"
#include "scanpgm.h"


typedef struct QuadSearchTree{
    QuadSearchTree* supleft;   
    QuadSearchTree* supright;
    QuadSearchTree* infright;
    QuadSearchTree* infleft;
    Point3D coord_supleft;
    Point3D coord_supright;
    Point3D coord_infright;
    Point3D coord_infleft;
    int nb_point;
} QuadSearchTree;

void initNode(QuadSearchTree* quadtree, Point3D coord_supleft, Point3D coord_supright, Point3D coord_infright, Point3D coord_infleft, int nombrePts);

void divideIntoFour(QuadSearchTree* quadtree, timacParams* timac, PGMImage* pgm);

bool isLeaf(QuadSearchTree* quadtree);

#endif
