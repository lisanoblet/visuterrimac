#ifndef VISU_H
#define VISU_H

#include <stdlib.h>
#include <math.h>
#include <stdio.h>

#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>
#include <iostream>

using namespace std;

#include "../include/scanpgm.h"

#define STEP_ANGLE	M_PI/90.
#define STEP_PROF	M_PI/90.
/* variables globales pour la gestion de la caméra */
extern float profondeur;
extern float latitude;
extern float longitude;

/* variables globales pour la gestion de l'objet */
extern float obj_rot;

/* Déclaration des fonctions */
void idle(PGMImage* pgm, timacParams* timac);


#endif
