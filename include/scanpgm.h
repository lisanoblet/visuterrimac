// Programme c++ to read a PGMB image
// and print its parameters

#ifndef SCANPGM_H
#define SCANPGM_H

#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <vector>
using namespace std;
// au lieu d'avoir un malloc à chaque pixel, on a un vecteur qui contient le malloc et qui se change dynamiquement


// Structure for storing the
// image data
typedef struct {
	char pgmType[3];
	std::vector<int> data; //un vector c'est un "tableau à une dimension"
	unsigned int width;
	unsigned int height;
	unsigned int maxValue;

//comme le vector est à une seule dimension, on utilise la fonction pour récupérer l'autre coordonnée
    int get(int i, int j) {
        //return 0;
        //std::cout << data[i * width + j] << std::endl;
        return data[i * width + j];
        
    }

} PGMImage;


typedef struct {
    string height_map;
    float xsize;
    float ysize;
    float zmin;
    float zmax;
    float znear;
    float zfar;
    float fov;
} timacParams;





// Function to open the input a PGM
// file and process it
bool openPGM(PGMImage* pgm, timacParams* timac);

// Function to print the file details in terrain.timac
//void printImageDetails(PGMImage* pgm, const char* ipfile, const char* fichier_timac);

void lireTimacParams(timacParams* params, char* ipfile);



#endif