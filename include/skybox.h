#ifndef SKYBOX_H
#define SKYBOX_H

#include <stdlib.h>
#include <math.h>
#include <stdio.h>
//#include <SDL/SDL.h>

//#include "stb_image.h"
#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>
#include "scanpgm.h"
#include "create_object.h"

typedef struct Image {
    int width;
    int height;
    unsigned char* data;
}Image;

GLuint definitionSkybox();

void displaySkybox(GLuint texturesSkybox[6], Camera* cam, timacParams* timac);
void loadTextures(GLuint texturesSkybox[6], GLuint textureEau[1], GLuint textureArbre[1]);

GLuint createTexture(char* path);


#endif
