#ifndef CREATE_OBJECT_H
#define CREATE_OBJECT_H

#include "scanpgm.h"
#include "geometry.h"
#include "quadtree.h"

struct Camera {
    Point3D position;
    float znear;
    float zfar;
    float fov;
    float longitude;
    float latitude;
    float profondeur;
    Vector3D up;
    Vector3D leftside;
    Vector3D rightside;
    Vector3D endside;
};

struct Sun {
    Point3D origin;
    Point3D position;
    Color3f color;
    float longitude;
    float radius;
    bool moving;
};

void createCoordinates(PGMImage* pgm, timacParams* timac);
void drawSquaretest(PGMImage* pgm, timacParams* timac, int filled);
Color3f quelleCouleur(float z);
void drawQuadTree(QuadSearchTree* quadtree, Camera* cam, PGMImage* pgm, timacParams* timac, int filled, Sun* sun);
void drawTriangles(Point3D point1, Point3D point2, Point3D point3, Point3D point4, PGMImage* pgm, timacParams* timac, int filled, Sun* sun);

//FONCTION CAMERA
void initCamera(Camera* cam, timacParams* timac);
bool visible(Camera* cam, Point3D supleft, Point3D supright, Point3D infright, Point3D infleft);
bool verif_intersect(Point3D position, Vector3D side, Point3D points[4]);


void initSun(Sun* sun, timacParams* timac);
Color3f getLight(Sun* sun, Point3D supleft, Point3D supright, Point3D infright, Point3D infleft);
void deplacementCam(Camera* cam, Vector3D directionRegard, Vector3D vecteurUp, Vector3D vecteurLeft, Vector3D deplacement);
Color3f getLightTriangle(Sun* sun, Point3D a, Point3D b, Point3D c);

Color3f getLightColumn(Sun* sun);

bool LOD(Camera* cam, QuadSearchTree* quadtree);
#endif