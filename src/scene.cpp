#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>
#include <tgmath.h>
#include "../include/scene.h"
#include "../include/visu.h"
#include "../include/create_object.h"
#include "../include/skybox.h"


Tree createTree(Vec3f positionTree, float sizeX, float sizeY){
    Tree arbre;
    arbre.positionTree = positionTree;
    arbre.sizeX = sizeX;
    arbre.sizeY = sizeY;
    return arbre;
}

// DESSIN REPERE
/*
void glDrawRepere(float length) {
	glBegin(GL_LINES);
		glColor3f(1.,0.,0.);
		glVertex3f(0.,0.,0.);
		glVertex3f(length,0.,0.);
		glColor3f(0.,1.,0.);
		glVertex3i(0.,0.,0.);
		glVertex3i(0.,length,0.);
		glColor3f(0.,0.,1.);
		glVertex3i(0.,0.,0.);
		glVertex3i(0.,0.,length);
	glEnd();
}*/

// DESSIN DE L'EAU
void glDrawWater(PGMImage* pgm, float hauteur, GLuint textureEau[1], Sun* sun) {

	Point3D point1 = createPoint(0, 0, hauteur);
	Point3D point2 = createPoint(0, pgm->height, hauteur);
	Point3D point3 = createPoint(pgm->width, pgm->height, hauteur);
	Point3D point4 = createPoint(pgm->width, 0, hauteur);

	Color3f lighting = getLight(sun, point2, point1, point4, point3);
            
	glEnable(GL_TEXTURE_2D); 
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, textureEau[0]);
	glBegin(GL_POLYGON);
		glColor3f(lighting.r, lighting.g, lighting.b);
	 	//glColor3f(0.56, 0.87, 0.93);
 		glTexCoord2f(0,0); glVertex3f(0, 0, hauteur); // top left
		glTexCoord2f(0,1); glVertex3f(0, pgm->height, hauteur); // top right 
		glTexCoord2f(1,1); glVertex3f(pgm->width, pgm->width, hauteur); // bottom right
		glTexCoord2f(1,0); glVertex3f(pgm->width, 0, hauteur);
 	glEnd();
 	glPopMatrix();
 glDisable(GL_TEXTURE_2D); 
}


//INITIALISATION DES ARBRES
void initTabRandom(int tabRandomX[TAILLE_TAB_RANDOM], int tabRandomY[TAILLE_TAB_RANDOM], int tabSize[TAILLE_TAB_RANDOM], timacParams* timac){
	for(int i = 0; i < TAILLE_TAB_RANDOM; i++){
		tabRandomX[i] = rand()%((int) timac->xsize-1);
		tabRandomY[i] = rand()%((int) timac->ysize-1);
		tabSize[i] = rand()%((int) 10);
	}
}

// CREATION DES COLONNES (ARBRES)
void displayTree(Tree* arbre, timacParams* timac, PGMImage* pgm, Camera* cam, GLuint textureArbre[1], Sun* sun){

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND); 
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA); 

          glPushMatrix();
            glBindTexture(GL_TEXTURE_2D, textureArbre[0]);

            float hauteurTerrain = pgm->get(arbre->positionTree.x, arbre->positionTree.y) / (float) pgm->maxValue * timac->zmax;

            Point3D point1 = createPoint(-arbre->sizeX/2, 0, -arbre->sizeY);

            Point3D point2 = createPoint(arbre->sizeX/2, 0, -arbre->sizeY);

            Point3D point3 = createPoint(arbre->sizeX/2, 0, arbre->sizeY);

            Point3D point4 = createPoint(-arbre->sizeX/2, 0, arbre->sizeY);

            float vecteurCamArbreX = arbre->positionTree.x - cam->position.x;
            float vecteurCamArbreY = arbre->positionTree.y - cam->position.y;
            float angle = atan2(vecteurCamArbreY, vecteurCamArbreX);

            Color3f lighting = getLightColumn(sun);
            glTranslatef(arbre->positionTree.x, arbre->positionTree.y, arbre->positionTree.z /* + hauteurTerrain */ + arbre->sizeY -0.5);
            glRotatef(angle*180/M_PI + 90, 0.0f, 0.0f, 1.0f );
           
              glBegin(GL_QUADS);
              glColor3f(lighting.r, lighting.g, lighting.b);
              //glColor3f(1.0, 1.0, 1.0);
             
                     glTexCoord2f(0, 1);
                    glVertex3f(point1.x, point1.y, point1.z);
                
                    glTexCoord2f(1, 1);
                    glVertex3f(point2.x, point2.y, point2.z);
                
                    glTexCoord2f(1, 0);
                    glVertex3f(point3.x, point3.y, point3.z);
                
                    glTexCoord2f(0, 0);
                    glVertex3f(point4.x, point4.y, point4.z); 
             
              glEnd();
          glPopMatrix();

 
}