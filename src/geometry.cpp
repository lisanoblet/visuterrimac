#include "../include/geometry.h"
#include <math.h>
#include <stdio.h>

// Construit le point (x, y, z)
Point3D createPoint(float x, float y, float z){
    Point3D nouveau_point;
    nouveau_point.x = x;
    nouveau_point.y = y;
    nouveau_point.z = z;
    return nouveau_point;
}


// Construit le vecteur (x, y, z)
Vector3D createVector(float x, float y, float z){
    Vector3D nouveau_vecteur;
    nouveau_vecteur.x = x;
    nouveau_vecteur.y = y;
    nouveau_vecteur.z = z;
    return nouveau_vecteur;
}

Color3f createColor(float x, float y, float z){
    Color3f nouvelle_couleur;
    nouvelle_couleur.r = x;
    nouvelle_couleur.g = y;
    nouvelle_couleur.b = z;
    return nouvelle_couleur;
}


// Construit le vecteur reliant les points P1 et P2
Vector3D createVectorFromPoints(Point3D p1, Point3D p2){
    Vector3D nouveau_vecteur;
    nouveau_vecteur.x = p2.x - p1.x;
    nouveau_vecteur.y = p2.y - p1.y;
    nouveau_vecteur.z = p2.z - p1.z;
    return nouveau_vecteur;
}


// Construit le point P + V (i.e. translation de P par V)
Point3D pointPlusVector(Point3D p, Vector3D v){
    Point3D nouveau_point;
    nouveau_point.x = p.x + v.x;
    nouveau_point.y = p.y + v.y;
    nouveau_point.z = p.z + v.z;
    return nouveau_point;
}


// Addition et soustraction des vecteurs V1 et V2
Vector3D addVectors(Vector3D v1, Vector3D v2){
    Vector3D nouveau_vecteur;
    nouveau_vecteur.x = v1.x + v2.x;
    nouveau_vecteur.y = v1.y + v2.y;
    nouveau_vecteur.z = v1.z + v2.z;
    return nouveau_vecteur;
}

Vector3D subVectors(Vector3D v1, Vector3D v2){
    Vector3D nouveau_vecteur;
    nouveau_vecteur.x = v1.x - v2.x;
    nouveau_vecteur.y = v1.y - v2.y;
    nouveau_vecteur.z = v1.z - v2.z;
    return nouveau_vecteur;
}


// Multiplication et division d'un vecteur V par un scalaire a
Vector3D multVector(Vector3D v, float a){
    v.x = v.x * a;
    v.y = v.y * a;
    v.z = v.z * a;
    return v;
}

Vector3D divVector(Vector3D v, float a){
    if(a == 0){
    return v;
}
    v.x = v.x / a;
    v.y = v.y / a;
    v.z = v.z / a;
    return v;
}


// Produit scalaire des vecteurs V1 et V2
float dot(Vector3D a, Vector3D b){
    return a.x * b.x + a.y * b.y + a.z * b.z;
}


// Norme d'un vecteur V
float norm(Vector3D v){
    return sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}


// Construit le vecteur normalisé du vecteur V
Vector3D normalize(Vector3D v){
    float longueur = norm(v);

if(longueur == 0){
    return v;
}

    v.x = v.x / longueur;
    v.y = v.y / longueur;
    v.z = v.z / longueur;
    return v;
}


void printPoint3D(Point3D p){
    printf("P(%f, %f, %f)", p.x, p.y, p.z);
}


void printVector3D(Vector3D v){
    printf("v(%f, %f, %f)", v.x, v.y, v.z);
}



Vector3D crossProduct(Vector3D v1, Vector3D v2){
    Vector3D vecteur_ortho;
    vecteur_ortho.x = v1.y * v2.z - v1.z * v2.y;
    vecteur_ortho.y = v1.z * v2.x - v1.x * v2.z;
    vecteur_ortho.z = v1.x * v2.y - v1.y * v2.x;
    return vecteur_ortho;
}

Color3f multColor(Color3f c, float a) {
    c.r *= a;
    c.g *= a;
    c.b *= a;
    return c;
}


float determinant(Vector3D a, Vector3D b){
    return a.x*b.y-a.y*b.x;
}

/*
bool intersectionVect(Vector3D v1, Vector3D v2){
    //int t;
    if(v1.y!=0 && (v1.x*v2.y-v2.x*v1.y)!=0){
        //t=(v1.y*(p2.x-p1.x)-v1.x*(p2.y-p2.x))/(v1.x*v2.y-v2.x*v1.y);
        return true;
    }
    else if (v2.y!=0){
        //t=(p2.x-p2.y)/v2.y;
        return true;
    }
    return false;
}


bool intersectionCam(Camera* cam, QuadSearchtree* quadtree){
    Vector3D left = createVectorFromPoints(quadtree->coord_supleft, quadtree->coord_infleft);
    Vector3D up = createVectorFromPoints(quadtree->coord_supleft, quadtree->coord_supright);
    Vector3D right = createVectorFromPoints(quadtree->coord_supright, quadtree->coord_infright);
    Vector3D down = createVectorFromPoints(quadtree->coord_infleft, quadtree->coord_infright);
    return intersectionVect(cam->leftside, left) 
        || intersectionVect(am->leftside, up) 
        || intersectionVect(cam->leftside, right) 
        || intersectionVect(cam->leftside, down)
        || intersectionVect(cam->rightside, left) 
        || intersectionVect(cam->rightside, up) 
        || intersectionVect(cam->rightside, right) 
        || intersectionVect(cam->rightside, down)
        || intersectionVect(cam->endside, left) 
        || intersectionVect(cam->endside, up) 
        || intersectionVect(cam->endside, right) 
        || intersectionVect(cam->endside, down);
}*/

