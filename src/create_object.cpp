#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>
#include <math.h>


#include "../include/create_object.h"
#include "../include/scanpgm.h"
#include "../include/quadtree.h"
#include "../include/geometry.h"

/*
void drawSquaretest(PGMImage* pgm, timacParams* timac, int filled) 
{
    //glColor3f(1.0, 1.0, 1.0);
    if(filled) 
    {
        //glColor3f(1.0, 0.0, 0.0);
        glBegin(GL_TRIANGLE_STRIP);

    }
    else 
    {
        glBegin(GL_LINE_STRIP);
    }
    for(int y = 0; y < pgm->height - 1; y++){
        for(int x = 0; x < pgm->width - 1; x++){

            float point1Z = pgm->get(x, y) / (float) pgm->maxValue * timac->zmax;
            float point2Z = pgm->get(x+1, y) / (float) pgm->maxValue * timac->zmax;
            float point3Z = pgm->get(x, y+1) / (float) pgm->maxValue * timac->zmax;
            float point4Z = pgm->get(x+1, y+1) / (float) pgm->maxValue * timac->zmax;

            

            Color3f Col1 = quelleCouleur(point1Z);
            glColor3f(Col1.r, Col1.g, Col1.b);
            glVertex3f(x, y, point1Z);
            Color3f Col2 = quelleCouleur(point2Z);
            glColor3f(Col2.r, Col2.g, Col2.b);
            glVertex3f(x+1 , y, point2Z);
            Color3f Col3 = quelleCouleur(point3Z);
            //cout << Col3.r << endl;
            glColor3f(Col3.r, Col3.g, Col3.b);
            glVertex3f(x, y+1 , point3Z);
            Color3f Col4 = quelleCouleur(point4Z);
            glColor3f(Col4.r, Col4.g, Col4.b);
            glVertex3f(x+1, y+1 , point4Z);
        }
    };



    glEnd();
}*/

Color3f quelleCouleur(float z){
    Color3f new_color;
    if(z > 4){
        new_color.r = 1.00;
        new_color.g = 0.97;
        new_color.b = 0.96;
        return new_color;
        //glColor3f(1.0, 0.0, 0.0);
    }
    else if (z > 3){
        new_color.r = 1.0;
        new_color.g = 0.89;
        new_color.b = 0.85;
        return new_color;
        //glColor3f(0.0, 1.0, 0.0);
    }
    else if (z > 2){
        new_color.r = 0.82;
        new_color.g = 0.77;
        new_color.b = 0.81;
        return new_color;
        //glColor3f(1.0, 1.0, 0.0);
    }
    else if (z > 1){
        new_color.r = 0.67;
        new_color.g = 0.68;
        new_color.b = 0.76;
        return new_color;
        //glColor3f(1.0, 0.0, 1.0);
    }
    else{
        new_color.r = 0.48;
        new_color.g = 0.50;
        new_color.b = 0.61;
    } 
}

//VERSION QUADTREE
void drawQuadTree(QuadSearchTree* quadtree, Camera* cam, PGMImage* pgm, timacParams* timac, int filled, Sun* sun) 
{
//cout << "1" << endl;
    if (!visible(cam, quadtree->coord_supleft, quadtree->coord_supright, quadtree->coord_infright, quadtree->coord_infleft)){
        return;
    }
    
    if (quadtree->nb_point<=4){
        drawTriangles(quadtree->coord_supleft, quadtree->coord_supright, quadtree->coord_infleft, quadtree->coord_infright, pgm, timac, filled, sun);
    }
    else{
        if(!isLeaf(quadtree)){
            
            drawQuadTree(quadtree->supleft, cam, pgm, timac, filled, sun);
            drawQuadTree(quadtree->supright, cam, pgm, timac, filled, sun);
            drawQuadTree(quadtree->infright, cam, pgm, timac, filled, sun);
            drawQuadTree(quadtree->infleft, cam, pgm, timac, filled, sun);
        }
       
    }
    /*
    if (!LOD(cam, quadtree)){
        drawTriangles(quadtree->coord_supleft, quadtree->coord_supright, quadtree->coord_infleft, quadtree->coord_infright, pgm, timac, filled, sun);
    }
    else {
        if(!isLeaf(quadtree)){   
            drawQuadTree(quadtree->supleft, cam, pgm, timac, filled, sun);
            drawQuadTree(quadtree->supright, cam, pgm, timac, filled, sun);
            drawQuadTree(quadtree->infright, cam, pgm, timac, filled, sun);
            drawQuadTree(quadtree->infleft, cam, pgm, timac, filled, sun);
        }
    }*/
}

void drawTriangles(Point3D supleft, Point3D supright, Point3D infleft, Point3D infright, PGMImage* pgm, timacParams* timac, int filled, Sun* sun){
    if(filled) 
        {  
            glBegin(GL_TRIANGLE_STRIP);
                Point3D supleft2 = createPoint(supleft.x, supleft.y, pgm->get(supleft.x, supleft.y) / (float) pgm->maxValue * timac->zmax);
                Point3D supright2 = createPoint(supright.x, supright.y, pgm->get(supright.x, supright.y) / (float) pgm->maxValue * timac->zmax);
                Point3D infleft2 = createPoint(infleft.x, infleft.y, pgm->get(infleft.x, infleft.y) / (float) pgm->maxValue * timac->zmax);
                Point3D infright2 = createPoint(infright.x, infright.y, pgm->get(infright.x, infright.y) / (float) pgm->maxValue * timac->zmax);

                Color3f lighting = getLightTriangle(sun, supleft2, supright2, infleft2);
                glColor3f(lighting.r, lighting.g, lighting.b);
                glVertex3f(supleft2.x, supleft2.y, supleft2.z);
                glVertex3f(supright2.x, supright2.y, supright2.z);
                glVertex3f(infleft2.x, infleft2.y, infleft2.z);

                Color3f lighting2 = getLightTriangle(sun, supright2, infright2, infleft2);
                glColor3f(lighting2.r, lighting2.g, lighting2.b);
                glVertex3f(supright2.x, supright2.y, supright2.z);
                glVertex3f(infleft2.x, infleft2.y, infleft2.z);
                
                glVertex3f(infright2.x, infright2.y, infright2.z);
            glEnd();
        }
    else 
        {
            glBegin(GL_LINE_STRIP);
                float point1Z = pgm->get(supleft.x, supleft.y) / (float) pgm->maxValue * timac->zmax;
                float point2Z = pgm->get(supright.x, supright.y) / (float) pgm->maxValue * timac->zmax;
                float point3Z = pgm->get(infleft.x, infleft.y) / (float) pgm->maxValue * timac->zmax;
                float point4Z = pgm->get(infright.x, infright.y) / (float) pgm->maxValue * timac->zmax;

               
                glVertex3f(supleft.x, supleft.y, point1Z);
               
                glVertex3f(supright.x, supright.y, point2Z);
                
                glVertex3f(infleft.x, infleft.y, point3Z);
                
                glVertex3f(infright.x, infright.y, point4Z);
            glEnd();
        }
      
}



void initCamera(Camera* cam, timacParams* timac){
    //cam->position = position;
    cam->znear = timac->znear;
    cam->zfar = timac->zfar;
    cam->fov = timac->fov* M_PI/180; //on définit le fov à 60
    cam->longitude = 0;
    cam->profondeur = 0;
    cam->latitude = M_PI/2.0;
    cam->up = createVector(0.,0.,1.);
}

bool visible(Camera* cam, Point3D supleft, Point3D supright, Point3D infright, Point3D infleft){
    float demi_fov= cam->fov/2; // régler le 0.2
    float hypotenuse= sqrt(cam->zfar*cam->zfar + (tan(demi_fov)*cam->zfar)*(tan(demi_fov)*cam->zfar));

    Point3D cameraPoint = createPoint(
        cam->position.x,
        cam->position.y,
        cam->position.z);

    Point3D leftPoint = createPoint(
        cam->position.x + hypotenuse*cos(cam->longitude + demi_fov+ 2*M_PI/3),
        cam->position.y + hypotenuse*sin(cam->longitude + demi_fov+ 2*M_PI/3),
        cam->position.z);

    Point3D rightPoint = createPoint(
        cam->position.x + hypotenuse*cos(cam->longitude - demi_fov+ 2*M_PI/3),
        cam->position.y + hypotenuse*sin(cam->longitude - demi_fov+ 2*M_PI/3),
        cam->position.z);

    cam->leftside=createVectorFromPoints(cameraPoint, leftPoint);
    cam->rightside=createVectorFromPoints(cameraPoint, rightPoint);
    cam->endside=createVectorFromPoints(leftPoint, rightPoint);

    Point3D corners[4]={supleft, supright, infright, infleft};

    if(verif_intersect(cameraPoint, cam->leftside, corners)){
        return false;
    }
    else if(verif_intersect(cameraPoint, cam->rightside, corners)){
        return false;
    }
    else if(verif_intersect(cameraPoint, cam->endside, corners)){
        return false;
    }
    else return true;
}


bool verif_intersect(Point3D position, Vector3D side, Point3D points[4]){
    int indice=0;
    while (indice<4){
        if(determinant(side, createVectorFromPoints(position, points[indice]))< 0){
            return false;
        }
        indice++;
    }
    return true;
}

void initSun(Sun* sun, timacParams* timac){
    sun->origin.x = timac->xsize/2;
    sun->origin.y = timac->ysize/2;
    sun->origin.z = 0;
    sun->color = createColor(1.0, 1.0, 1.0);
    sun->longitude = -M_PI/6;
    sun->radius = timac->ysize;
    sun->position.x = sun->origin.x;
    sun->position.y = sun->origin.y - sun->radius * cos(sun->longitude);
    sun->position.z = sun->origin.z - sun->radius * sin(sun->longitude);
    sun->moving = false;
}

// LUMIERE
Color3f getLight(Sun* sun, Point3D supleft, Point3D supright, Point3D infright, Point3D infleft){
    Vector3D diag1 = createVectorFromPoints(supleft, infright);
    Vector3D diag2 = createVectorFromPoints(supright, infleft);
    Vector3D normale = normalize(crossProduct(diag1, diag2));
    Point3D centre = createPoint((diag1.x + diag2.x)/2, (diag1.y + diag2.y)/2, (diag1.z + diag2.z)/2);
    Vector3D vectSunshine = createVectorFromPoints(sun->position, centre);
    Color3f quadLight = multColor(sun->color, -dot(normale, normalize(vectSunshine))); 
    return quadLight; 
}

// LUMIERE POUR LES COLONNES (ARBRES)
Color3f getLightColumn(Sun* sun){
    return multColor(sun->color, -sin(sun->longitude)); 
}

Color3f getLightTriangle(Sun* sun, Point3D a, Point3D b, Point3D c){
    Vector3D v1 = createVectorFromPoints(a,b);
    Vector3D v2 = createVectorFromPoints(a,c);
    Vector3D normale = normalize(crossProduct(v1,v2));
    Point3D centre = createPoint((a.x+b.x+c.x)/3, (a.y+b.y+c.y)/3, (a.z+b.z+c.z)/3);
    Vector3D vectSunshine = createVectorFromPoints(sun->position, centre);
    Color3f couleurz = quelleCouleur(centre.z);
    Color3f triangleLight = multColor(couleurz, -dot(normale, normalize(vectSunshine))); 
    return triangleLight;
}

bool LOD(Camera* cam, QuadSearchTree* quadtree){
    Point3D centre = createPoint((quadtree->coord_supleft.x+quadtree->coord_supright.x)/2, (quadtree->coord_supleft.y+quadtree->coord_infleft.y)/2, (quadtree->coord_supleft.z+quadtree->coord_supright.z+quadtree->coord_infleft.z+quadtree->coord_infright.z)/4);
    float distance = sqrt((centre.x-cam->position.x)*(centre.x-cam->position.x)+(centre.y-cam->position.y)*(centre.y-cam->position.y)+(centre.z-cam->position.z)*(centre.z-cam->position.z));
    if (distance<200){
        return true;
    }
    return false;
}