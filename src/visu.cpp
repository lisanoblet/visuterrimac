
#define _USE_MATH_DEFINES

#include <stdlib.h>
#include <math.h>
#include <stdio.h>

#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>

#include "../include/visu.h"
#include "../include/create_object.h"
#include "../include/scene.h"
#include "../include/geometry.h"
#include "../include/scanpgm.h"
#include "../include/skybox.h"
#include "../include/quadtree.h"
	
/* variables globales pour la gestion de la caméra */
float profondeur = 3;
float latitude = M_PI/2.0;
float longitude = 0.0;

float obj_rot = 0.0;

float theta = 0.0;
float phi = -M_PI;

float posCamX = -5.0;
float posCamY = 1.0;
float posCamZ = 0.0;

PGMImage* pgm;
timacParams* timac;
QuadSearchTree* quadtree;
Camera* cam;
Sun *sun;

GLuint texturesSkybox[6];
GLuint textureEau[1];
GLuint textureArbre[1];

#define TAILLE_TAB_RANDOM 50
int tabRandomX[TAILLE_TAB_RANDOM];
int tabRandomY[TAILLE_TAB_RANDOM];
int tabSize[TAILLE_TAB_RANDOM];

int countArbre;

/*********************************************************/
/* fonction de dessin de la scène à l'écran              */
static void drawFunc() { 

	/* reinitialisation des buffers : couleur et ZBuffer */
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/* modification de la matrice de la scène */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	/* Debut du dessin de la scène */
	glPushMatrix();
	

	/* INITIALISATION DES PARAMETRES GL */

	glDisable(GL_DEPTH_TEST);
        
        glDisable(GL_LIGHTING);
    
	//INITIALISATION DE LA SKYBOX
		displaySkybox(texturesSkybox, cam, timac);

       glEnable(GL_DEPTH_TEST);
        
    
	// INITIALISATION DE LA CAMERA
	Vec3f positionCam = createVector(posCamX+pgm->width/2, posCamY + pgm->height/2, posCamZ + 10.0);
	Vec3f directionRegard = multVector(normalize(createVector(cos(cam->longitude)*sin(cam->latitude),sin(cam->longitude)*sin(cam->latitude),cos(cam->latitude))), 3.0);
	
	Vec3f vecteurLeft = normalize(createVector(cos(cam->longitude + M_PI/2), sin(cam->longitude + M_PI/2), 0));
	Vec3f vecteurUp = normalize(crossProduct(directionRegard, vecteurLeft));
	
	cam->position = positionCam;

	
	gluLookAt(cam->position.x, cam->position.y, cam->position.z,
			  cam->position.x + directionRegard.x, cam->position.y + directionRegard.y, cam->position.z + directionRegard.z,
			  cam->up.x, cam->up.y, cam->up.z); 

	
	//glDrawRepere(25.0);

	/* 

	float position[4] = {5.0,5.0,5.0,1.0};
	float black[3] = {0.0,0.0,0.0};
	float intensite[3] = {1000.0,1000.0,1000.0};
	//glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0,GL_POSITION,position);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,intensite);
	glLightfv(GL_LIGHT0,GL_SPECULAR,black);
	//glLightf(GL_LIGHT0,GL_,black);
	//glLightf(GL_LIGHT0,GL_SPECULAR,black); */

	glPushMatrix();
	
	// DESSINS
	//EAU
	glDrawWater(pgm, 1.8, textureEau, sun);
	//QUADTREE
	drawQuadTree(quadtree, cam, pgm, timac, 1, sun);
		
	//COLONNES (ARBRE)
	for(int i = 0; i < countArbre; i++){
        
		Point3D positionTree = createPoint(tabRandomX[i], tabRandomY[i], pgm->get(tabRandomX[i], tabRandomY[i])/(float) pgm->maxValue * timac->zmax);
	
			float sizeX = tabSize[i];
			float sizeY = tabSize[i]*3;
			Tree arbre = createTree(positionTree, sizeX, sizeY);
	
			displayTree(&arbre, timac, pgm, cam, textureArbre, sun);

		}


	glDisable(GL_LIGHTING);

	/* Fin du dessin */
	glPopMatrix();

	/* fin de la définition de la scène */
	glFinish();

	/* changement de buffer d'affichage */
	glutSwapBuffers();
}


/*********************************************************/
/* fonction de changement de dimension de la fenetre     */
/* paramètres :                                          */
/* - width : largeur (x) de la zone de visualisation     */
/* - height : hauteur (y) de la zone de visualisation    */
static void reshapeFunc(int width,int height) {
	GLfloat  h = (GLfloat) width / (GLfloat) height ;
	
	/* dimension de l'écran GL */
	glViewport(0, 0, (GLint)width, (GLint)height);
	/* construction de la matrice de projection */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	/* définition de la camera */
	gluPerspective(timac->fov, h, timac->znear, timac->zfar);			// Angle de vue, rapport largeur/hauteur, near, far

	/* Retour a la pile de matrice Modelview
			et effacement de celle-ci */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

/*********************************************************/
/* fonction associée aux interruptions clavier           */
/* paramètres :                                          */
/* - c : caractère saisi                                 */
/* - x,y : coordonnée du curseur dans la fenêtre         */
static void kbdFunc(unsigned char c, int x, int y) {
	/* sortie du programme si utilisation des touches ESC, */
	/* 'q' ou 'Q'*/

	Vec3f directionRegard = multVector(normalize(createVector(cos(cam->longitude)*sin(cam->latitude),sin(cam->longitude)*sin(cam->latitude),cos(cam->latitude))), 3.0);
	
	Vec3f vecteurLeft = normalize(createVector(cos(cam->longitude + M_PI/2), sin(cam->longitude + M_PI/2), 0));
	Vec3f vecteurUp = normalize(crossProduct(directionRegard, vecteurLeft));

	Vec3f deplacement = createVector(0.0, 0.0, 0.0);


	switch(c) {
		case 27 :
			delete(cam);
			delete(quadtree);
			delete(timac);
			delete(pgm);
			exit(0);
			break;
		case 'F' : case 'f' : glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
			break;
		case 'P' : case 'p' : glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
			break;
		//case 'R' : case 'r' : glutIdleFunc(idle);
		//	break;
		case 'I' : case 'i' : glutIdleFunc(NULL);
			break;

		//pour les mouvements de caméra
		//devant
		
		case 'Z' : case 'z' : deplacement.y += 0.5*sin(cam->latitude);
			//deplacement.x += 0.5 * cos(cam->latitude);
			break;
		//derrière
		
		case 'S' : case 's' : deplacement.y -= 0.5*sin(cam->latitude);
			//deplacement.x -= 0.5 * cos(cam->latitude);
			break;
		//droite
		case 'D' : case 'd' : deplacement.x += 0.5*cos(cam->latitude + M_PI/2);
			//deplacement.y -= 0.5 * sin(cam->latitude + M_PI/2);
			break;
		//gauche
		case 'Q' : case 'q' : deplacement.x -= 0.5*cos(cam->latitude + M_PI/2);
			//deplacement.y += 0.5 * sin(cam->latitude + M_PI/2);
			break;
			//haut
		case 'E' : case 'e' : deplacement.z -= 0.5;
			break;
		//bas
		case 'A' : case 'a' : deplacement.z += 0.5;
			break;

		//+ arbre
		case 'G' : case 'g' : 
			if(countArbre == 50){
				cout <<"Il y a trop de colonnes sur le terrain !" << endl;
			}
			else{
				countArbre += 1;
			}
			
			break;
			
		//- arbre
		case 'H' : case 'h' :
			if(countArbre == 0){
				cout <<"Il n'y a plus de colonnes sur le terrain !" << endl;
			}
			else{
				countArbre -= 1;
			}
			break;
		//soleil
		case 'Y' : case 'y' :
			if(sun->moving){
				sun->moving = false;
			}else{
				sun->moving = true;
			}
			break;
		case 'U' : case 'u' : 
			if(!sun->moving){
				sun->longitude += 0.05;
				sun->position.y = sun->origin.y - sun->radius * cos(sun->longitude);
				sun->position.z = sun->origin.z - sun->radius * sin(sun->longitude);
			}
			break;
		case 'T' : case 't' : 
			if(!sun->moving){
				sun->longitude -= 0.05;
				sun->position.y = sun->origin.y - sun->radius * cos(sun->longitude);
				sun->position.z = sun->origin.z - sun->radius * sin(sun->longitude);
			}
			break;	

		default:
			printf("Appui sur la touche %c\n",c);
	};
	
	//deplacementCam(cam, directionRegard, vecteurUp, vecteurLeft, deplacement);
	posCamX += deplacement.y * directionRegard.x + deplacement.z * vecteurUp.x + deplacement.x * vecteurLeft.x;
	posCamY += deplacement.y * directionRegard.y + deplacement.z * vecteurUp.y + deplacement.x * vecteurLeft.y;
	posCamZ += deplacement.y * directionRegard.z + deplacement.z * vecteurUp.z + deplacement.x * vecteurLeft.z;
 
	cam->position.x = posCamX;
	cam->position.y = posCamY;
	cam->position.z = posCamZ;

	glutPostRedisplay();
}

/*********************************************************/
/* fonction associée aux interruptions clavier pour les  */
/*          touches spéciales                            */
/* paramètres :                                          */
/* - c : code de la touche saisie                        */
/* - x,y : coordonnée du curseur dans la fenêtre         */
static void kbdSpFunc(int c, int x, int y) {
	/* sortie du programme si utilisation des touches ESC, */
	switch(c) {
		case GLUT_KEY_UP :
			if (cam->latitude>STEP_ANGLE) cam->latitude -= STEP_ANGLE;
			break;
		case GLUT_KEY_DOWN :
			if(cam->latitude<M_PI-STEP_ANGLE) cam->latitude += STEP_ANGLE;
			break;
		case GLUT_KEY_LEFT :
			cam->longitude += STEP_ANGLE;
			break;
		case GLUT_KEY_RIGHT :
			cam->longitude -= STEP_ANGLE;
			break;
		case GLUT_KEY_PAGE_UP :
			cam->profondeur += STEP_PROF;
			break;
		case GLUT_KEY_PAGE_DOWN :
			if (cam->profondeur>0.1+STEP_PROF) cam->profondeur -= STEP_PROF;
			break;
	

		default:
			printf("Appui sur une touche spéciale\n");
	}
	//printf("%f", cam->longitude);
	glutPostRedisplay();
}


/*********************************************************/
/* fonction associée au clique de la souris              */
/* paramètres :                                          */
/* - button : nom du bouton pressé GLUT_LEFT_BUTTON,     */
/*   GLUT_MIDDLE_BUTTON ou GLUT_RIGHT_BUTTON             */
/* - state : état du bouton button GLUT_DOWN ou GLUT_UP  */
/* - x,y : coordonnées du curseur dans la fenêtre        */
static void mouseFunc(int button, int state, int x, int y) { 
}

/*********************************************************/
/* fonction associée au déplacement de la souris bouton  */
/* enfoncé.                                              */
/* paramètres :                                          */
/* - x,y : coordonnées du curseur dans la fenêtre        */
static void motionFunc(int x, int y) { 
}

/*********************************************************/
/* fonction d'initialisation des paramètres de rendu et  */
/* des objets de la scènes.                              */
static void init(PGMImage* pgm, timacParams* timac) {
	

	obj_rot = 0.0;

	/* INITIALISATION DES PARAMETRES GL */
	/* couleur du fond (gris sombre) */
	glClearColor(0.3,0.3,0.3,0.0);
	/* activation du ZBuffer */
	glEnable( GL_DEPTH_TEST);

	loadTextures(texturesSkybox, textureEau, textureArbre);


	/* lissage des couleurs sur les facettes */
	glShadeModel(GL_SMOOTH);

	/* INITIALISATION DE LA SCENE */

}

void idle(void) {
	//obj_rot+=STEP_ANGLE;
	if(sun->moving){
		sun->longitude -= 0.2 * STEP_ANGLE;
		sun->position.y = sun->origin.y - sun->radius * cos(sun->longitude);
		sun->position.z = sun->origin.z - sun->radius * sin(sun->longitude);
	}

	glutPostRedisplay();
}

int main(int argc, char** argv) {

	pgm = new PGMImage();
    char* ipfile;
    

    if (argc == 2)
        ipfile = argv[1];
    else
        ipfile = "terrain.timac";

    printf("\tip file : %s\n", ipfile);

    // Process the image and print
    // its details
	timac = new timacParams();


	lireTimacParams(timac, ipfile);


    if (openPGM(pgm, timac)){
       
    }
	
	sun = new Sun();
	initSun(sun, timac);

	cam = new Camera();
	initCamera(cam, timac);
	quadtree = new QuadSearchTree();
	Point3D supleft = createPoint(0, 0, pgm->get(0, 0) / (float) pgm->maxValue * timac->zmax);
    Point3D supright = createPoint(pgm->width-1, 0, pgm->get(pgm->width-1, 0) / (float) pgm->maxValue * timac->zmax);
    Point3D infright = createPoint(pgm->width-1, pgm->height-1, pgm->get(pgm->width-1, pgm->height-1) / (float) pgm->maxValue * timac->zmax);
    Point3D infleft = createPoint(0, pgm->height, pgm->get(0, pgm->height) / (float) pgm->maxValue * timac->zmax);
	int quantitePts = (pgm->width * pgm->height);
	initNode(quadtree, supleft, supright, infright, infleft, quantitePts);
	
	divideIntoFour(quadtree, timac, pgm);
	
	countArbre = 5;
	initTabRandom(tabRandomX, tabRandomY, tabSize, timac);


  
	/* traitement des paramètres du programme propres à GL */
	glutInit(&argc, argv);
	/* initialisation du mode d'affichage :                */
	/* RVB + ZBuffer + Double buffer.                      */
	glutInitDisplayMode(GLUT_RGBA|GLUT_DEPTH|GLUT_DOUBLE);
	/* placement et dimentions originales de la fenêtre */
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(750, 750);
	/* ouverture de la fenêtre */
	if (glutCreateWindow("VISUTERRIMAC - Lisa & Robin") == GL_FALSE) {
		return 1;
	}

	init(pgm, timac);

	/* association de la fonction callback de redimensionnement */
	glutReshapeFunc(reshapeFunc);
	/* association de la fonction callback d'affichage */
	glutDisplayFunc(drawFunc);
	/* association de la fonction callback d'événement du clavier */
	glutKeyboardFunc(kbdFunc);
	/* association de la fonction callback d'événement du clavier (touches spéciales) */
	glutSpecialFunc(kbdSpFunc);
	/* association de la fonction callback d'événement souris */
	glutMouseFunc(mouseFunc);
	/* association de la fonction callback de DRAG de la souris */
	glutMotionFunc(motionFunc);

	glutIdleFunc(idle);

	/* boucle principale de gestion des événements */
	glutMainLoop();
	/* Cette partie du code n'est jamais atteinte */
	
	return 0;
}
