
#include "../include/quadtree.h"


//On initialise le QuadTree
void initNode(QuadSearchTree* quadtree, Point3D coord_supleft, Point3D coord_supright, Point3D coord_infright, Point3D coord_infleft, int nombrePts){
    //Initialisation des enfants
    quadtree->supleft = nullptr;
    quadtree->supright = nullptr;
    quadtree->infright = nullptr;
    quadtree->infleft = nullptr;

    //Initialisation des coins
    quadtree->coord_supleft = coord_supleft;
    quadtree->coord_supright = coord_supright;
    quadtree->coord_infright = coord_infright;
    quadtree->coord_infleft = coord_infleft;
    //Initialisation du nombre de point
    quadtree->nb_point = nombrePts;
}

void divideIntoFour(QuadSearchTree* quadtree, timacParams* timac, PGMImage* pgm) {

        if(quadtree->nb_point == 4){
            return;
        }

        if(isLeaf(quadtree) && quadtree->nb_point>4){
            int x1= quadtree->coord_supleft.x;
            int x2= quadtree->coord_supright.x;
            int y1= quadtree->coord_supleft.y;
            int y2= quadtree->coord_infleft.y;
            int x= (x1 + x2)/2;
            int y= (y1 + y2)/2;
            int new_nombrePts = quadtree->nb_point / 4;
        
            //Creation du noeud superieur gauche
            quadtree->supleft = new QuadSearchTree();
            initNode(
                quadtree->supleft,
                quadtree->coord_supleft,
                createPoint(x, y1, pgm->get(x, y1)),
                createPoint(x, y, pgm->get(x, y)),
                createPoint(x1, y, pgm->get(x1, y)), 
                new_nombrePts            
            );

            //Creation du noeud superieur droit
            
            quadtree->supright = new QuadSearchTree();
            initNode(
                quadtree->supright,
                createPoint(x, y1, pgm->get(x, y1)),
                quadtree->coord_supright,
                createPoint(x2, y, pgm->get(x2, y)),
                createPoint(x, y, pgm->get(x, y)), 
                new_nombrePts   
            );

            //Creation du noeud inferieur droit
            quadtree->infright = new QuadSearchTree();
            initNode(
                quadtree->infright,
                createPoint(x, y, pgm->get(x, y)),
                createPoint(x2, y, pgm->get(x2, y)),
                quadtree->coord_infright,
                createPoint(x, y2, pgm->get(x, y2)), 
                new_nombrePts   
            );

            //Creation du noeud inferieur gauche
            quadtree->infleft = new QuadSearchTree();
            initNode(
                quadtree->infleft,
                createPoint(x1, y, pgm->get(x1, y)),
                createPoint(x, y, pgm->get(x, y)),
                createPoint(x, y2, pgm->get(x, y2)),
                quadtree->coord_infleft, 
                new_nombrePts   
            );
            //cout << "pleaseeeee" << endl;
            divideIntoFour(quadtree->supright, timac, pgm);
            divideIntoFour(quadtree->infright, timac, pgm);
            divideIntoFour(quadtree->supleft, timac, pgm);
            divideIntoFour(quadtree->infleft, timac, pgm);
        }
}

bool isLeaf(QuadSearchTree* quadtree) {
    // return True if the node is a leaf (it has no children)
    if (!quadtree->supright && !quadtree->infright && !quadtree->supleft && !quadtree->infleft){
            return true;
    }
    return false;
}