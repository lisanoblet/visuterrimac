
//https://learnopengl.com/Advanced-OpenGL/Cubemaps
//https://raptor.developpez.com/tutorial/opengl/skybox/
//#define STB_IMAGE_IMPLEMENTATION
//#include "../include/stb_image.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include <string.h>
#include <iostream>
//#include <SDL/SDL.h>
#include "../include/skybox.h"
#include "../include/visu.h"



#define GL_TEXTURE_CUBE_MAP_ARB             0x8513
#define GL_TEXTURE_CUBE_MAP_POSITIVE_X_ARB  0x8515
#define GL_TEXTURE_CUBE_MAP_NEGATIVE_X_ARB  0x8516
#define GL_TEXTURE_CUBE_MAP_POSITIVE_Y_ARB  0x8517
#define GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_ARB  0x8518
#define GL_TEXTURE_CUBE_MAP_POSITIVE_Z_ARB  0x8519
#define GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_ARB  0x851A




void loadTextures(GLuint texturesSkybox[6], GLuint textureEau[1], GLuint textureArbre[1]){
    char* liens_textures[8];

    // Textures de la skybox
    liens_textures[0] = (char*) "doc/right.jpg";
    liens_textures[1] = (char*) "doc/left.jpg";
    liens_textures[2] = (char*) "doc/front.jpg";
    liens_textures[3] = (char*) "doc/back.jpg";
    liens_textures[4] = (char*) "doc/top.jpg";
    liens_textures[5] = (char*) "doc/bottom.jpg";

    // Texture de l'eau
    liens_textures[6] = (char*) "doc/eau2.jpg";

    // Texture de l'arbre
    liens_textures[7] = (char*) "doc/column.png";
    
  

    for(int i = 0; i < 6; i++){
      texturesSkybox[i] = createTexture(liens_textures[i]);
    }

    //for(int i = 0; i < 1; i++){
      textureEau[0] = createTexture(liens_textures[6]);
    //}
    textureArbre[0] = createTexture(liens_textures[7]);
}




GLuint createTexture(char* chemin){
    SDL_Surface* image = IMG_Load(chemin);
    if(NULL == image) {
        fprintf(stderr, "Echec du chargement de l'image %s\n", chemin);
        exit(EXIT_FAILURE);
    }

    GLuint texture_id;
    glGenTextures(1, &texture_id);

    glBindTexture(GL_TEXTURE_2D, texture_id);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    GLenum format;
    switch(image->format->BytesPerPixel) {
        case 1:
            format = GL_RED;
            break;
        case 3:
            format = GL_RGB;
            break;
        case 4:
            format = GL_RGBA;
            break;
        default:
            fprintf(stderr, "Format des pixels de l'image %s non supporte.\n", chemin);
            return EXIT_FAILURE;
    }

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image->w, image->h, 0, format, GL_UNSIGNED_BYTE, image->pixels);

    glBindTexture(GL_TEXTURE_2D, 0);  
    
    SDL_FreeSurface(image);

    return texture_id;

} 

 
void displaySkybox(GLuint texturesSkybox[6], Camera* cam, timacParams* timac){
int x = 0;
int y = 0;
int z = 0;
    float distance = 10.0;
    glEnable(GL_TEXTURE_2D); 
    glDepthMask(GL_FALSE);
    
    // Réglage de l'orientation
    glPushMatrix();
    glLoadIdentity();
   
    glRotatef(cam->latitude*180/M_PI, 1.0f, 0.0f, 0.0f );
    glRotatef(cam->longitude*180/M_PI, 0.0f, 0.0f, 1.0f );
  
    glColor3f(1.0, 1.0, 1.0);
    
    // Rendu de la géométrie
    glBindTexture(GL_TEXTURE_2D, texturesSkybox[3]);
    
    glBegin(GL_QUADS);          // blanche
        glTexCoord2f(0,0); glVertex3f (distance + x, distance + y, -distance + z);
        glTexCoord2f(1,0); glVertex3f (-distance + x, distance + y, -distance + z);
        glTexCoord2f(1,1); glVertex3f (-distance + x, distance + y, distance + z);
        glTexCoord2f(0,1); glVertex3f (distance + x, distance + y, distance + z);
    glEnd();
    //glColor3f(1.0, 0.0, 0.0);
    glBindTexture(GL_TEXTURE_2D, texturesSkybox[1]);
    
    glBegin(GL_QUADS);          // rouge
        glTexCoord2f(0,0); glVertex3f (-distance + x, distance + y, -distance + z);
        glTexCoord2f(1,0); glVertex3f (-distance + x, -distance + y, -distance + z);
        glTexCoord2f(1,1); glVertex3f (-distance + x, -distance + y, distance + z);
        glTexCoord2f(0,1); glVertex3f (-distance + x, distance + y, distance + z);
    glEnd();
    
    //glColor3f(0.0, 1.0, 0.0);
    glBindTexture(GL_TEXTURE_2D, texturesSkybox[0]);
    
    glBegin(GL_QUADS);          //vert
        glTexCoord2f(0,0); glVertex3f (distance + x, -distance + y, -distance + z);
        glTexCoord2f(1,0); glVertex3f (distance + x, distance + y, -distance + z);
        glTexCoord2f(1,1); glVertex3f (distance + x, distance + y, distance + z);
        glTexCoord2f(0,1); glVertex3f (distance + x, -distance + y, distance + z);
    glEnd();
    //glColor3f(0.0, 1.0, 1.0);
     glBindTexture(GL_TEXTURE_2D, texturesSkybox[5]);
    
    glBegin(GL_QUADS);           //cyan     BOTTOM
        glTexCoord2f(1,1); glVertex3f (distance + x, distance + y, distance + z);
        glTexCoord2f(0,1); glVertex3f (-distance + x, distance + y, distance + z);
        glTexCoord2f(0,0); glVertex3f (-distance + x, -distance + y, distance + z);
        glTexCoord2f(1,0); glVertex3f (distance + x, -distance + y, distance + z);
    glEnd();
    //glColor3f(1.0, 0.0, 1.0);
    glBindTexture(GL_TEXTURE_2D, texturesSkybox[4]);
    
    glBegin(GL_QUADS);            //magenta     TOP
        glTexCoord2f(1,0); glVertex3f (distance + x, distance + y, -distance + z);
        glTexCoord2f(0,0); glVertex3f (-distance + x, distance + y, -distance + z);
        glTexCoord2f(0,1); glVertex3f (-distance + x, -distance + y, -distance + z);
        glTexCoord2f(1,1); glVertex3f (distance + x, -distance + y, -distance + z);
    glEnd();

    //glColor3f(1.0, 1.0, 0.0);
    glBindTexture(GL_TEXTURE_2D, texturesSkybox[2]);
    
    glBegin(GL_QUADS);           //jaune     
        glTexCoord2f(0,0); glVertex3f (-distance, -distance , -distance );
        glTexCoord2f(1,0); glVertex3f (distance , -distance , -distance );
        glTexCoord2f(1,1); glVertex3f (distance, -distance , distance );
        glTexCoord2f(0,1); glVertex3f (-distance, -distance , distance );
    glEnd(); 
    // Réinitialisation de la matrice ModelView
    glPopMatrix();
    glDepthMask(GL_TRUE);
    glDisable(GL_TEXTURE_2D); 

}





