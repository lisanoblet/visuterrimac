// Programme pour lire le fichier pgm en c++ et écrire ses paramètres dans .timac

#include "../include/scanpgm.h"


// Fonction pour ouvrir le fichier et process les données 
bool openPGM(PGMImage* pgm, timacParams* timac)
{
	// Open the image file in the
	// 'read binary' mode
	std::ifstream f;
	f.open(timac->height_map);

	if (!f.is_open()){
        cout << "erreur" << endl;
	return false;}

    std::string ligne;
    bool is_header_read = false;
    bool is_size_read = false;
    bool is_color_depth_read = false;

	while(std::getline(f, ligne)) {
        if (ligne.find('#') == std::string::npos) {
            if (!is_header_read) {
                is_header_read = true;
            }
            else {
                if (!is_size_read) {
                    is_size_read = true;
                    std::string width_string = ligne.substr(0, ligne.find(' ')); //si on trouve des espaces, on prend le chiffre avant
                    std::string heigth_string = ligne.substr(ligne.find(' ')+1,ligne.length()); 
                    pgm->width = std::stoi(width_string);
                
                    pgm->height = std::stoi(heigth_string);
                    pgm->data.reserve(pgm->width * pgm->height); //on alloue la taille au vecteur 
                }
                else {
                    if (!is_color_depth_read) {
                        is_color_depth_read = true;
                        pgm->maxValue = std::stoi(ligne);   
                      
                        int i;

                        while (f >> i) {
                            pgm->data.push_back(i);
                        }                  
                    }
                }
            }
        }
    }
	return true;
}

// Function to print the file details in terrain.timac
/*
void printImageDetails(PGMImage* pgm, const char* ipfile, const char* fichier_timac)
{
    FILE* timacfile = fopen(fichier_timac, "w");

    if (timacfile == NULL){
        printf("Error!");
        exit(1);
    }
    //On donne le nom du fichier
    fprintf(timacfile, "%s %s %s", "char height_map = ", ipfile, ";\n");

    //On recupere les dimensions
    fprintf(timacfile, "%d", "float xsize = ", pgm->width, ";\n");
    fprintf(timacfile, "%s %d %s", "float ysize = ", pgm->height, ";\n");
    fprintf(timacfile, "%s %d %s", "float zmin = ", 0, ";\n");
    fprintf(timacfile, "%s %d %s", "float zmax = ", pgm->maxValue, ";\n");
    fprintf(timacfile, "%s %d %s", "float znear = ", 0, ";\n");
    fprintf(timacfile, "%s %d %s", "float zfar = ", 10, ";\n");
    fprintf(timacfile, "%s %s %s", "float fov = ", "(2*PI)/3", ";\n");
    

	// close file
    fclose(timacfile);
}*/


void lireTimacParams(timacParams* params, char* ipfile){

    std::ifstream file;
	file.open(ipfile);
    //std::ifstream file("params.timac");
    if(file) {
       
        file >> params->height_map;
        file >> params->xsize;
        file >> params->ysize;
        file >> params->zmin;
        file >> params->zmax;
        file >> params->znear;
        file >> params->zfar;

        file >> params->fov;

    }else{
        std::cerr << "l'ouverture n'a pas marché" << std::endl;
    }
}
