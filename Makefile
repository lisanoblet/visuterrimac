# $(BIN) est le nom du binaire généré
BIN = visu
# FLAG
FLAGS = -Wall -O3 -lSDL2 -lGLU -lGL -lm -g -Wextra
# INCLUDES
INC = -I./
# INCLUDES
LIBS = -lglut -lGLEW -lGLU -lGL -lm -lSDL2_image -lSDL2
# INCLUDES
LIBDIR =
# Compilateur
CPP = g++
# $(OBJECTS) sont les objets qui seront générés après la compilation
OBJECTS = visu.o gldrawing.o create_object.o geometry.o scanpgm.o structures.o scene.o quadtree.o skybox.o

BIN_DIR	= bin
INC_DIR = -I include
SRC_DIR	= src
OBJ_DIR	= obj

SRC_FILES 	= $(shell find $(SRC_DIR)/ -type f -name '*.cpp')
OBJ_FILES 	= $(patsubst $(SRC_DIR)/%.cpp, $(OBJ_DIR)/%.o, $(SRC_FILES))
EXEC_BIN	= visuterrimacLR


default: $(BIN)

all: $(OBJ_FILES)

#$(BIN): $(OBJECTS)
#	@echo "**** PHASE DE LIEN ****"
#	$(CPP) $(OBJECTS) $(FLAGS) -o $(BIN) $(LIBDIR) $(LIBS) 

visu : $(OBJ_FILES)
	#@mkdir -p $(BIN_DIR)/
	$(CPP) -o $(EXEC_BIN) $(OBJ_FILES) $(LIBS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	mkdir -p "$(@D)"
	$(CPP) -c $< -o $@ $(FLAGS) $(INC_DIR)

clean:
	rm -rf *~
	rm -rf $(SRC_DIR)/*/*~
	rm -rf $(OBJ_DIR)/
	rm -rf $(BIN_DIR)/*

